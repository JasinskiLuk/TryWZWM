﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TryWZWM.Core.DTOs;
using TryWZWM.DAL.Data;

namespace TryWZWM.Presentation.API.WZWM_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApartmentsController : ControllerBase
    {
        private readonly WZWMContext _context;

        public ApartmentsController(WZWMContext context)
        {
            _context = context;
        }

        // GET: api/Apartment
        [HttpGet]
        public async Task<IActionResult> GetApartments()
        {
            IEnumerable<ApartmentDTO> apartments = Enumerable.Empty<ApartmentDTO>();

            try
            {
                apartments = await _context.Apartments.ToListAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok(apartments);
        }

        // GET: api/Apartment/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetApartment(int id)
        {
            ApartmentDTO apartment = null;

            try
            {
                apartment = await _context.Apartments.FindAsync(id);

                if (apartment == null)
                    return NotFound("Apartment not found.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok(apartment);
        }

        // PUT: api/Apartment/5
        [HttpPut]
        public async Task<IActionResult> PutApartment(ApartmentDTO apartment)
        {
            try
            {
                _context.Entry(apartment).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.Apartments.Any(e => e.Id == apartment.Id))
                    {
                        return NotFound("Apartment not found.");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return NoContent();
        }

        // POST: api/Apartment
        [HttpPost]
        public async Task<IActionResult> PostApartment(ApartmentDTO apartment)
        {
            try
            {
                _context.Apartments.Add(apartment);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return CreatedAtAction("GetApartment", new { id = apartment.Id }, apartment);
        }

        // DELETE: api/Apartment/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteApartment(int id)
        {
            try
            {
                ApartmentDTO apartment = await _context.Apartments.FindAsync(id);

                if (apartment == null)
                    return NotFound("Apartment not found.");

                _context.Apartments.Remove(apartment);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return NoContent();
        }
    }
}
