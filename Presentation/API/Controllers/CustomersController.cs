﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TryWZWM.Core.DTOs;
using TryWZWM.DAL.Data;

namespace TryWZWM.Presentation.API.WZWM_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly WZWMContext _context;

        public CustomersController(WZWMContext context)
        {
            _context = context;
        }

        // GET: api/Customer
        [HttpGet]
        public async Task<IActionResult> GetCustomers()
        {
            IEnumerable<CustomerDTO> customers = Enumerable.Empty<CustomerDTO>();

            try
            {
                customers = await _context.Customers.ToListAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok(customers);
        }

        // GET: api/Customer/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomer(int id)
        {
            CustomerDTO customer = null;

            try
            {
                customer = await _context.Customers.FindAsync(id);

                if (customer == null)
                    return NotFound("Customer not found.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok(customer);
        }

        // PUT: api/Customer/5
        [HttpPut]
        public async Task<IActionResult> PutCustomer(CustomerDTO customer)
        {
            try
            {
                _context.Entry(customer).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.Customers.Any(e => e.Id == customer.Id))
                    {
                        return NotFound("Customer not found.");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return NoContent();
        }

        // POST: api/Customer
        [HttpPost]
        public async Task<IActionResult> PostCustomer(CustomerDTO customer)
        {
            try
            {
                _context.Customers.Add(customer);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return CreatedAtAction("GetCustomer", new { id = customer.Id }, customer);
        }

        // DELETE: api/Customer/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCustomer(int id)
        {
            try
            {
                CustomerDTO customer = await _context.Customers.FindAsync(id);

                if (customer == null)
                    return NotFound("Customer not found.");

                _context.Customers.Remove(customer);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return NoContent();
        }
    }
}