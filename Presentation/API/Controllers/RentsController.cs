﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TryWZWM.Core.DTOs;
using TryWZWM.DAL.Data;

namespace TryWZWM.Presentation.API.WZWM_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RentsController : ControllerBase
    {
        private readonly WZWMContext _context;

        public RentsController(WZWMContext context)
        {
            _context = context;
        }
        // GET: api/Rent
        [HttpGet]
        public async Task<IActionResult> GetRents()
        {
            IEnumerable<RentDTO> rents = Enumerable.Empty<RentDTO>();

            try
            {
                rents = await _context.Rents.ToListAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok(rents);
        }

        // GET: api/Rent/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRent(int id)
        {
            RentDTO rent = null;

            try
            {
                rent = await _context.Rents.FindAsync(id);

                if (rent == null)
                    return NotFound("Rent not found.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok(rent);
        }

        // PUT: api/Rent/5
        [HttpPut]
        public async Task<IActionResult> PutRent(RentDTO rent)
        {
            try
            {
                _context.Entry(rent).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.Rents.Any(e => e.Id == rent.Id))
                    {
                        return NotFound("Rent not found.");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return NoContent();
        }

        // POST: api/Rent
        [HttpPost]
        public async Task<IActionResult> PostRent(RentDTO rent)
        {
            try
            {
                _context.Rents.Add(rent);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return CreatedAtAction("GetRent", new { id = rent.Id }, rent);
        }

        // DELETE: api/Rent/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRent(int id)
        {
            try
            {
                RentDTO rent = await _context.Rents.FindAsync(id);

                if (rent == null)
                    return NotFound("Rent not found.");

                _context.Rents.Remove(rent);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return NoContent();
        }
    }
}
