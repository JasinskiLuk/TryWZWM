﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TryWZWM.Core.DTOs;
using TryWZWM.DAL.Data;

namespace TryWZWM.Presentation.API.WZWM_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private readonly WZWMContext _context;
        public LocationsController(WZWMContext context)
        {
            _context = context;
        }

        // GET: api/Location
        [HttpGet]
        public async Task<IActionResult> GetLocations()
        {
            IEnumerable<LocationDTO> locations = Enumerable.Empty<LocationDTO>();

            try
            {
                locations = await _context.Locations.ToListAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok(locations);
        }

        // GET: api/Location/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetLocation(int id)
        {
            LocationDTO location = null;

            try
            {
                location = await _context.Locations.FindAsync(id);

                if (location == null)
                    return NotFound("Location not found.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return Ok(location);
        }

        // PUT: api/Location/5
        [HttpPut]
        public async Task<IActionResult> PutLocation(LocationDTO location)
        {
            try
            {
                _context.Entry(location).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_context.Locations.Any(e => e.Id == location.Id))
                    {
                        return NotFound("Location not found.");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return NoContent();
        }

        // POST: api/Location
        [HttpPost]
        public async Task<IActionResult> PostLocation(LocationDTO location)
        {
            try
            {
                _context.Locations.Add(location);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return CreatedAtAction("GetLocation", new { id = location.Id }, location);
        }

        // DELETE: api/Location/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteLocation(int id)
        {
            try
            {
                LocationDTO location = await _context.Locations.FindAsync(id);

                if (location == null)
                    return NotFound("Location not found.");

                _context.Locations.Remove(location);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }

            return NoContent();
        }
    }
}
