﻿namespace TryWZWM.Core.DTOs
{
    public class CustomerDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long PESEL { get; set; }
        public int Phone { get; set; }
        public string Email { get; set; }
    }
}