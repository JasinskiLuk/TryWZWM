﻿namespace TryWZWM.Core.DTOs
{
    public class ApartmentDTO
    {
        public int Id { get; set; }
        public LocationDTO Location { get; set; }
        public string BuildingNumber { get; set; }
        public string ApartmentNumber { get; set; }
        public decimal Area { get; set; }
        public bool IsRent { get; set; }
    }
}