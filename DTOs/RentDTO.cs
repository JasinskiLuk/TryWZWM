﻿using System;

namespace TryWZWM.Core.DTOs
{
    public class RentDTO
    {
        public int Id { get; set; }
        public CustomerDTO Customer { get; set; }
        public ApartmentDTO Apartment { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}