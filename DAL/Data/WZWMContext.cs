﻿using Microsoft.EntityFrameworkCore;
using TryWZWM.Core.DTOs;

namespace TryWZWM.DAL.Data
{
    public class WZWMContext : DbContext
    {
        public WZWMContext(DbContextOptions<WZWMContext> options) : base(options) { }

        public DbSet<ApartmentDTO> Apartments { get; set; }
        public DbSet<CustomerDTO> Customers { get; set; }
        public DbSet<LocationDTO> Locations { get; set; }
        public DbSet<RentDTO> Rents { get; set; }
    }
}